package nl.utwente.di.celsiusToFahrenheit;

public class Converter {
    public double getFahrenheit(String celcius) {
        double tempature = Double.parseDouble(celcius);
        return tempature * 1.8 + 32;
    }
}
