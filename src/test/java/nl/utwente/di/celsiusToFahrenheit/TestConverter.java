package nl.utwente.di.celsiusToFahrenheit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
        /***
        Tests the Converter
        */

public class TestConverter {
    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();
        double price = converter.getFahrenheit("1");
        Assertions.assertEquals(10.0, price, 0.0, "Price of book 1");
    }
}

